import os
import cv2
import torch
import torch.nn
import torch.utils.data
import torchvision.transforms as transforms

import numpy as np
import matplotlib
import matplotlib.image
from tqdm import tqdm
from sqlitedict import SqliteDict
from numpy.linalg import eig, inv


class H18train_set(torch.utils.data.Dataset):

    def __init__(self, fpath: str):
        self.dpath = os.path.join(fpath, 'training_set')
        self.fimages = list()

        listdir = os.listdir(self.dpath)
        for fimage in listdir:
            if '.png' in fimage:
                if '_Annotation' not in fimage:
                    if fimage[:-4] + '_Annotation.png' in listdir:
                        self.fimages.append(fimage)
                    else:
                        raise(ValueError('no Annotation image for {}'.format(fimage)))

        self.transform = transforms.Compose([
            transforms.ToPILImage(),
            transforms.Resize((384, 384)),
            transforms.ToTensor()
        ])

    def __len__(self):
        return len(self.fimages)

    def __getitem__(self, i):
        img = matplotlib.image.imread(
            os.path.join(self.dpath, self.fimages[i]))
        mask = matplotlib.image.imread(
            os.path.join(self.dpath, self.fimages[i][:-4] + '_Annotation.png'))
        if self.transform:
            # Add fantom channel
            img = self.transform(img)
            mask = self.transform(mask)
        return (img, mask)


class H18test_set(torch.utils.data.Dataset):

    def __init__(self, fpath: str):
        self.dpath = os.path.join(fpath, 'test_set')
        self.fimages = list()

        listdir = os.listdir(self.dpath)
        for fimage in listdir:
            if '.png' in fimage:
                if '_Annotation' not in fimage:
                    self.fimages.append(fimage)

        self.transform = transforms.Compose([
            transforms.ToPILImage(),
            transforms.Resize((384, 384)),
            transforms.ToTensor()
        ])

    def __len__(self):
        return len(self.fimages)

    def __getitem__(self, i):
        img = matplotlib.image.imread(
            os.path.join(self.dpath, self.fimages[i]))
        if self.transform:
            img = self.transform(img)
        return (img, self.fimages[i])


def dice_loss(inputs: torch.Tensor, target: torch.Tensor) -> float:
    iflat = inputs.view(-1)
    tflat = target.view(-1)
    intersection = (iflat * tflat).sum()

    return 1 - ((2. * intersection + 1) / (1 + iflat.sum() + tflat.sum()))


def train(net, criterion, optimizer, dataset,
          train_size=0.6, batch_size=8, max_epoch=30, fpath='.', name='model'):

    result = {}

    train_size = int(len(dataset) * train_size)

    train_data, test_data = torch.utils.data.random_split(dataset, [train_size, len(dataset) - train_size])

    train_generator = torch.utils.data.DataLoader(dataset=train_data, batch_size=batch_size, shuffle=True)
    test_generator = torch.utils.data.DataLoader(dataset=test_data, batch_size=batch_size, shuffle=True)

    optimizer = optimizer(net.parameters())

    epochs = tqdm(range(max_epoch))
    losses = np.zeros(max_epoch)
    val_losses = np.zeros(max_epoch)
    acc = np.zeros(max_epoch)
    val_acc = np.zeros(max_epoch)

    db = SqliteDict('db/trains.db', autocommit=True)

    # Training...
    for epoch in epochs:
        # Train on train dataset
        net.train()
        for batch in train_generator:
            imgs, masks = batch

            optimizer.zero_grad()
            outputs = net(imgs.cuda().float())
            loss = criterion(outputs.float(), masks.cuda().float())
            loss.backward()
            optimizer.step()

            losses[epoch] += loss.item()
            acc[epoch] += dice_loss(outputs, masks.cuda())
        # Evaluate loss on test \wo grad
        with torch.no_grad():
            for data in test_generator:
                images, masks = data
                outputs = net(images.cuda().float())
                val_losses[epoch] += criterion(outputs.float(), masks.cuda().float())
                val_acc[epoch] += dice_loss(outputs, masks.cuda())

        losses[epoch] /= len(train_generator)
        val_losses[epoch] /= len(test_generator)
        acc[epoch] /= len(train_generator)
        val_acc[epoch] /= len(test_generator)

        epochs.set_description("loss: {} ----- val_loss: {} ---- val_acc: {}".format(
            round(losses[epoch], 3),
            round(val_losses[epoch], 3),
            round(val_acc[epoch], 3),
        ))

        torch.save(net, os.path.join(fpath, name + '_{}.pt'.format(epoch)))

        result['loss'] = losses
        result['val_loss'] = val_losses
        result['acc'] = acc
        result['val_acc'] = val_acc

        db[name] = result

    return result


def imshow(images: np.ndarray, masks: np.ndarray, predictions=None, only=False):
    import pylab as plt
    from matplotlib import cm

    for i in range(masks.shape[0]):
        img = images[i]
        mask = masks[i]

        fig, ax = plt.subplots(ncols=2)
        if not only:
            ax[0].imshow(img.numpy()[0, :, :], cmap=cm.hot)
        ax[0].imshow(mask.numpy()[0, :, :], alpha=0.3)
        if predictions is not None:
            prediction = predictions[i]
            if not only:
                ax[1].imshow(img.numpy()[0, :, :], cmap=cm.hot)
            ax[1].imshow(prediction.numpy()[0, :, :], alpha=0.3)
        plt.show()


def predictEllipseValues(img: torch.Tensor, model) -> list:
    """
    Predict Ellipse params for US fetal head image
    :param img: image
    :param model: torch.model
    :return: [center, angle, [major, minor]]
    """
    mask = model(img)
    arr = mask.cpu().detach().numpy()[0,0]
    arr = cv2.resize(arr, dsize=(800, 540), interpolation=cv2.INTER_CUBIC)
    py, px = np.where(arr > 5e-4)
    res = fitEllipse(px, py)
    angle = ellipse_angle_of_rotation(res)
    major, minor = ellipse_axis_length(res)
    center = ellipse_center(res)
    return [center, angle, [major, minor]]


def fitEllipse(x,y):
    x = x[:, np.newaxis]
    y = y[:, np.newaxis]
    D = np.hstack((x*x, x*y, y*y, x, y, np.ones_like(x)))
    S = np.dot(D.T,D)
    C = np.zeros([6,6])
    C[0,2] = C[2,0] = 2; C[1,1] = -1
    E, V = eig(np.dot(inv(S), C))
    n = np.argmax(np.abs(E))
    a = V[:, n]
    return a


def ellipse_center(a):
    b,c,d,f,g,a = a[1]/2, a[2], a[3]/2, a[4]/2, a[5], a[0]
    num = b*b-a*c
    x0=(c*d-b*f)/num
    y0=(a*f-b*d)/num
    return np.array([x0,y0])


def ellipse_angle_of_rotation( a ):
    b,c,d,f,g,a = a[1]/2, a[2], a[3]/2, a[4]/2, a[5], a[0]
    return 0.5*np.arctan(2*b/(a-c))


def ellipse_axis_length( a ):
    b,c,d,f,g,a = a[1]/2, a[2], a[3]/2, a[4]/2, a[5], a[0]
    up = 2*(a*f*f+c*d*d+g*b*b-2*b*d*f-a*c*g)
    down1=(b*b-a*c)*( (c-a)*np.sqrt(1+4*b*b/((a-c)*(a-c)))-(c+a))
    down2=(b*b-a*c)*( (a-c)*np.sqrt(1+4*b*b/((a-c)*(a-c)))-(c+a))
    res1=np.sqrt(up/down1)
    res2=np.sqrt(up/down2)
    return np.array([res1, res2])