import torch
import torch.optim as optim
import torch.nn.functional as F
import argparse

from src.models.unet import UNet
from src.models.segnet import SegNet
from src.models.enet import ENet
from src.models.enet import BoxENet
from src.utils import train, H18test_set, H18train_set, dice_loss


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--model', type=str, help="""
        name of segmentaton net:
            unet, segnet, enet, boxenet
    """)
    parser.add_argument('--loss', type=str, help="""
        name of loss function:
            dice, bincrossentr
    """)

    args = parser.parse_args()

    DATA_PATH = './dataset'

    if args.loss == 'dice':
        criterion = dice_loss
    elif args.loss == 'bincrossentr':
        criterion = F.binary_cross_entropy
    else:
        raise(ValueError('wrong --loss'))

    if args.model == 'unet':
        net = UNet().cuda()
    elif args.model == 'segnet':
        net = SegNet().cuda()
    elif args.model == 'enet':
        net = ENet().cuda()
    elif args.model == 'boxenet':
        net = BoxENet().cuda()
    else:
        raise(ValueError('wrong --loss'))

    MODEL = args.model + '_' + args.loss

    print(torch.cuda.get_device_name(0))

    dataset = H18train_set(DATA_PATH)
    optimizer = optim.Adam

    result = train(
        net, criterion, optimizer, dataset,
        train_size=0.9, max_epoch=60,
        fpath='workspace/checkpoints',
        name=MODEL
    )


    print(
        """
        ######################################################
        ####################SUMMARY###########################
        ######################################################        
        """
    )
    for k in result.keys():
        print('{} : {}'.format(k, str(result[k][-1])))

